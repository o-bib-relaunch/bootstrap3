{**
 * templates/frontend/pages/search.tpl
 *
 * Copyright (c) 2014-2017 Simon Fraser University Library
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to search and view search results.
 *
 * @uses $query Value of the primary search query
 * @uses $authors Value of the authors search filter
 * @uses $dateFrom Value of the date from search filter (published after).
 *  Value is a single string: YYYY-MM-DD HH:MM:SS
 * @uses $dateTo Value of the date to search filter (published before).
 *  Value is a single string: YYYY-MM-DD HH:MM:SS
 * @uses $yearStart Earliest year that can be used in from/to filters
 * @uses $yearEnd Latest year that can be used in from/to filters
 *}
{include file="frontend/components/header.tpl" pageTitle="common.search"}
<script>
function show(id) {
    if(document.getElementById) {
        var mydiv = document.getElementById(id);
        mydiv.style.display = (mydiv.style.display=='block'?'none':'block');
    }
}

</script>

<div  id="main-content" class="page page_search">

	<div class="page-header">
		<h1>{translate key="common.search"}</h1>
	</div>

	{* Main Search From *}
	<form method="post" id="search-form" class="search-form" action="{url op="search"}" role="search">
		{csrf}
		
		<div id="id" class="form-group">
			{* Repeat the label text just so that screen readers have a clear
			   label/input relationship *}
			<label class="sr-only" for="query">
				{translate key="search.searchFor"}
			</label>

			<div class="input-group">
				<input type="text" id="query" name="query" value="{$query|escape}" class="query form-control" placeholder="{translate key="common.search"}">
				<span class="input-group-btn">
					<input type="submit" value="{translate key="common.search"}" class="btn btn-default">
				</span>
			</div>
		</div>

		<a href="" onclick="javascript:show('divText'); return false">
			<legend>
				{translate key="search.advancedFilters"}
				<acronym title="Mit einem Klick erweiterte Filtermöglichkeiten ein- oder ausblenden.">&#9432</acronym>
			</legend></a>
		<div style="display: block" id="divText"><fieldset class="search-advanced">
			<div  class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="dateFromYear">
							{translate key="search.dateFrom"}
						</label>
						<acronym title="Ein anderes Feld muss vorher ausgefüllt sein, bevor nach dem Datum gefiltert werden kann.">&#9432</acronym>						
						<div class="form-inline">
							<div class="form-group">
								{html_select_date prefix="dateFrom" time=$dateFrom start_year=$yearStart end_year=$yearEnd  field_order="DMY"}
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="dateToYear">
							{translate key="search.dateTo"}
						</label>
						<acronym title="Ein anderes Feld muss vorher ausgefüllt sein, bevor nach dem Datum gefiltert werden kann.">&#9432</acronym>
						<div class="form-inline">
							<div class="form-group">
								{html_select_date prefix="dateTo" time=$dateTo start_year=$yearStart end_year=$yearEnd month_empty={$smarty.now|date_format:"%B"} day_empty={$smarty.now|date_format:"%d"} field_order="DMY"}
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="authors">
							{translate key="search.author"}
						</label>
						<acronym title="Trunkierung ist rechts und links mit * möglich.">&#9432</acronym>
						<input class="form-control" type="text" for="authors" name="authors" value="{$authors|escape}">
					</div>
					<div class="form-group">
						<label for="title">
							{translate key="search.title"}
						</label>
						<acronym title="Trunkierung ist rechts und links mit * möglich. Untertitel werden auch durchsucht.">&#9432</acronym>
						<input class="form-control" type="text" for="title" name="title" value="{$title|escape}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="fullText">
							{translate key="search.fullText"}
						</label>
						<acronym title="Durchsucht den vollständigen Artikel nach einem bestimmten Wort, einer bestimmten Zeichenfolge.">&#9432</acronym>
						<input class="form-control" type="text" for="galleyFullText" name="galleyFullText" value="{$galleyFullText|escape}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="abstract">
							{translate key="search.abstract"}
						</label>
						<acronym title="Durchsucht das Abstract nach einem bestimmten Wort, einer bestimmten Zeichenfolge.">&#9432</acronym>
						<input class="form-control" type="text" for="abstract" name="abstract" value="{$abstract|escape}">
					</div>
				</div>
			</div>
		</fieldset>
		</div>

		{* Search results *}
		<div class="search-results">
			<h2>
				{translate key="search.searchResults"}
			</h2>
			{iterate from=results item=result}
				{include file="frontend/objects/article_summary.tpl" article=$result.publishedSubmission showDatePublished=true hideGalleys=true}
			{/iterate}
		</div>

		{* No results found *}
		{if $results->wasEmpty()}
			{if $error}
				{include file="frontend/components/notification.tpl" type="error" message=$error|escape}
			{else}
				{include file="frontend/components/notification.tpl" type="notice" messageKey="search.noResults"}
			{/if}

		{* Results pagination *}
		{else}
			<div class="cmp_pagination">
				{page_info iterator=$results}
				{page_links anchor="results" iterator=$results name="search" query=$query searchJournal=$searchJournal authors=$authors title=$title abstract=$abstract galleyFullText=$galleyFullText discipline=$discipline subject=$subject type=$type coverage=$coverage indexTerms=$indexTerms dateFromMonth=$dateFromMonth dateFromDay=$dateFromDay dateFromYear=$dateFromYear dateToMonth=$dateToMonth dateToDay=$dateToDay dateToYear=$dateToYear orderBy=$orderBy orderDir=$orderDir}
			</div>
		{/if}

	</form>
		<h2>
			Suchhinweise:
		</h2>
	<ul>
		<li>Groß- und Kleinschreibung der Suchbegriffe werden nicht unterschieden.</li>
		<li>Häufig vorkommende Worte werden ignoriert</li>
		<li>Standardmäßig werden nur die Artikel aufgelistet, die alle Suchbegriffe enthalten (implizites UND).</li>
		<li>Mehrere Suchbegriffe verbunden durch ODER ergeben Artikel, die den einen und/oder den anderen Begriff enthalten, z.B. Bildung ODER Forschung.</li>
		<li>Suchen Sie nach einer genauen Wortfolge, indem Sie sie in Anführungszeichen setzen, z.B. "Veröffentlichung mit freiem Zugang".</li>
		<li>Schließen Sie ein Wort aus, indem Sie ihm - oder NICHT voranstellen, z.B. Internetpublikation -Politik oder Internetpublikation NICHT Politik.</li>
		<li>Verwenden Sie * als Platzhalter für beliebige Zeichenfolgen, z.B. Sozi* Moral listet alle Dokumente, die "soziologisch" oder "sozial" enthalten.</li>
	</ul>
</div><!-- .page -->

{include file="common/frontend/footer.tpl"}
